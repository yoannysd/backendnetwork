package cu.musala.network.Repository;

import cu.musala.network.Entity.Gateway;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface GatewayRepository extends JpaRepository<Gateway, Integer> {
    Optional<Gateway> findFirstBySerialNumber(String serialNumber);
}
