@echo off

set ip_bd=localhost
set port_bd=5432
set nombre_bd=musala
set usuario_bd=postgres
set contrasenna_bd=postgres

set app_port=3001

echo -------------------------------------------------------

echo About server database
set ip_server_bd=0
set /p ip_server_bd="IP [%ip_bd%]: "
if %ip_server_bd% equ 0 (
set ip_server_bd=%ip_bd%
)

set port_server_bd=0
set /p port_server_bd="Port [%port_bd%]: "
if %port_server_bd% equ 0 (
set port_server_bd=%port_bd%
)

rem bd
set name_bd=0
set /p name_bd="Database name [%nombre_bd%]: "
if %name_bd% equ 0 (
set name_bd=%nombre_bd%
)

set user_bd=0
set /p user_bd="User [%usuario_bd%]: "
if %user_bd% equ 0 (
set user_bd=%usuario_bd%
)

set pass_bd=0
set /p pass_bd="Password [%contrasenna_bd%]: "
if %pass_bd% equ 0 (
set pass_bd=%contrasenna_bd%
)

echo -------------------------------------------------------
echo About the api rest
set application_port=0
set /p application_port="Port application [%app_port%]: "
if %application_port% equ 0 (
set application_port=%app_port%
)

echo -------------------------------------------------------

rem #Passing all variables to the jar.

start java -Xmx50M -jar BackendNetworkAPI-0.0.1.jar --port=%application_port% --database-ip-port=%ip_server_bd%:%port_server_bd% --database-name=%name_bd% --datasource.username=%user_bd% --datasource.password=%pass_bd%

echo Ready. Your application is running by the port %application_port% according your configuration.
