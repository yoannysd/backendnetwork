#!/bin/bash

CURENT_DIRECTORY=`dirname $0`

ip_bd=localhost
port_bd=5432
nombre_bd=musala
usuario_bd=postgres
contrasenna_bd=postgres

app_port=3001

echo "_______________________________________________________"

echo "About server database"
echo " "

echo "IP [$ip_bd]: "
read ip_server_bd
if [ -z "$ip_server_bd" ]
then
ip_server_bd="$ip_bd"
fi

echo "Port [$port_bd]: "
read port_server_bd
if [ -z "$port_server_bd" ]
then
port_server_bd="$port_bd"
fi

#bd
echo "Database name [$nombre_bd]: "
read name_bd
if [ -z "$name_bd" ]
then
name_bd="$nombre_bd"
fi

echo "User [$usuario_bd]: "
read user_bd
if [ -z "$user_bd" ]
then
user_bd="$usuario_bd"
fi

echo "Password [$contrasenna_bd]: "
read pass_bd
if [ -z "$pass_bd" ]
then
pass_bd="$contrasenna_bd"
fi

echo "_____________________________________________________"

echo "About the api rest"
echo " "

echo "Port application [$app_port]: "
read application_port
if [ -z "$application_port" ]
then
application_port="$app_port"
fi

echo ""
echo "Ready. Your application is running by the port ${application_port} according your configuration."

sleep 5

java -jar $CURENT_DIRECTORY/BackendNetworkAPI-0.0.1.jar --port=$application_port --database-ip-port=$ip_server_bd:$port_server_bd --database-name=$name_bd --datasource.username=$user_bd --datasource.password=$pass_bd
