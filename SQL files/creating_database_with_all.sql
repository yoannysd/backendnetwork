-- Database: musala

-- DROP DATABASE musala;

CREATE DATABASE musala
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8';







DROP SCHEMA public ;

-- SCHEMA: network

-- DROP SCHEMA network ;

CREATE SCHEMA network
    AUTHORIZATION postgres;







-- SEQUENCE: network.seq_gateway_id

-- DROP SEQUENCE network.seq_gateway_id;

CREATE SEQUENCE network.seq_gateway_id;

ALTER SEQUENCE network.seq_gateway_id
    OWNER TO postgres;







-- SEQUENCE: network.seq_peripheraldevice_uid

-- DROP SEQUENCE network.seq_peripheraldevice_uid;

CREATE SEQUENCE network.seq_peripheraldevice_uid;

ALTER SEQUENCE network.seq_peripheraldevice_uid
    OWNER TO postgres;







-- Table: network.gateway

-- DROP TABLE network.gateway;

CREATE TABLE network.gateway
(
    id integer NOT NULL DEFAULT nextval('network.seq_gateway_id'::regclass),
    serialnumber character varying COLLATE pg_catalog."default" NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    ipv4 character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT gateway_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE network.gateway
    OWNER to postgres;







-- Table: network.nstatus

-- DROP TABLE network.nstatus;

CREATE TABLE network.nstatus
(
    id integer NOT NULL,
    status character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT nstatus_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE network.nstatus
    OWNER to postgres;







-- Table: network.peripheraldevice

-- DROP TABLE network.peripheraldevice;

CREATE TABLE network.peripheraldevice
(
    uid bigint NOT NULL DEFAULT nextval('network.seq_peripheraldevice_uid'::regclass),
    vendor character varying COLLATE pg_catalog."default" NOT NULL,
    date date NOT NULL DEFAULT CURRENT_DATE,
    status integer NOT NULL DEFAULT 1,
    gatewayid integer,
    CONSTRAINT peripheraldivice_pkey PRIMARY KEY (uid),
    CONSTRAINT peripheraldivice_gatewayid_fkey FOREIGN KEY (gatewayid)
        REFERENCES network.gateway (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT peripheraldivice_status_fkey FOREIGN KEY (status)
        REFERENCES network.nstatus (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE network.peripheraldevice
    OWNER to postgres;







-- Inserting the status offline and online in nstatus
INSERT INTO network.nstatus(id,status) VALUES (1,'offline');
INSERT INTO network.nstatus(id,status) VALUES (2,'online');







-- Inserting some data of example in the table gateway
INSERT INTO network.gateway(serialnumber,name,ipv4) VALUES ('asdkjk234jkkjas','Gateway of the building 1', '10.56.12.35');
INSERT INTO network.gateway(serialnumber,name,ipv4) VALUES ('kljkj459992390023','Gateway of the building 2', '10.56.13.41');