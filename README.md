# BackendNetwork

Backend of the application

YOU CAN DOWNLOAD THE COMPILED BACKEND USING THIS URL: https://gitlab.com/api/v4/projects/21641191/jobs/artifacts/dev/download?job=build_network

====>Steps to RUN the Rest API:<====

1- The tester has to create a database in PostgreSQL at first. Once the database is created the use the file "SQL files"/creating_database_with_all.sql for creating the schema, tables and sequences needed.

2- The database "name", "host", "port", "user" and "password" for the connection that the api use by default are "musala"(db), "localhost"(host), "5432"(port), "postgres"(user) and "postgres"(password), anyway it can be passed another values when running the api as it is seen in the file application.properties of the project "api-network". (Or running the runners/runner.bat in windows or runners/runner.sh in linux, the file that is decided to use has to be in the same directory of the jar).

2- The default port of the api is "3001", anyway you can change that value by passing a different value when running as it is seen in the file application.properties. (Or running the runners/runner.bat in windows or runners/runner.sh in linux, the file that is decided to use has to be in the same directory of the jar).


====>Steps to TEST the Rest API:<====
(Change "localhost" and "3001" by the correct configuration that was used to run the api)

1- For getting all "Gateway" with all their data, you can make a GET request to localhost:3001/network/api/gateway.

2- For getting an specific "Gateway" with all its data, you can make a GET request to localhost:3001/network/api/gateway/1, where the last number is the id of the "Gateway".

3- For adding/associating a "Peripherical device" to a specific "Gateway", you can make a POST request to localhost:3001/network/api/peripheral/4, where the last number is the id of the "Gateway". The data of the "Peripherical device" can be specified in a json like this: {"vendor": "Microsoft Inc."}

4- For removing an specific "Peripherical device" from a "Gateway", you can make a DELETE request to localhost:3001/network/api/peripheral/90, where the last number is the id of the "Peripherical device".

5- For adding a "Gateway" with or without peripheral devices, you can make a POST request to localhost:3001/network/api/gateway . The data of the "Gateway" can be specified in a json like this: 
{
	"serialNumber": "12rt67933",
	"name": "Gateway from the building 145",
	"ipv4": "3.5.6.4",
	"peripheralDevices": [
		{
			"vendor": "Company ABCD"
		},
		{
			"vendor": "Company EFGH"
		}
	]
}

6- For removing a "Gateway", you can make a DELETE request to localhost:3001/network/api/gateway/24, where the last number is the id of the "Gateway".
